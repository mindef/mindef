var gulp = require('gulp');
var sass = require('gulp-sass');
var elixir = require('laravel-elixir');
var concat = require('gulp-concat');

require('laravel-elixir-sass-compass');

gulp.task('styles', function () {
    gulp.src('resources/assets/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('public/css/'));
});

gulp.task('watch', function () {
    gulp.watch('resources/assets/sass/*.scss', ['styles']);
    gulp.watch('resources/assets/javascript/*.js', ['scripts']);
});

gulp.task('scripts', function () {
    return gulp.src('resources/assets/javascript/*.js')
        .pipe(concat('app.js'))
        .pipe(gulp.dest('public/js/'));
});