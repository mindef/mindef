<?php

namespace App;


class DateTime extends \DateTime
{
    public static function formatDutch($date = null)
    {
        return DateTime::createFromFormat('Y-m-d', $date)->format('d-m-Y');
    }

    public static function formatDutchWithTime($date = null)
    {
        return DateTime::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
    }
}
