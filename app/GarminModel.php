<?php

namespace App;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;

class GarminModel extends Model
{

    private $server;

    public function __construct()
    {
        $this->server = new Garmin([
            'identifier' => env('CONSUMER_KEY'),
            'secret' => env('CONSUMER_SECRET'),
            'callback_uri' => env('CALLBACK_URI')
        ]);
    }

    public function getData()
    {
        $temporaryCredentials = $this->server->getTemporaryCredentials();
        session(['temporary_credentials' => serialize($temporaryCredentials)]);
        return $this->server->authorize($temporaryCredentials);
    }

    public function callback($request)
    {
        $sessionTempCredentials = session('temporary_credentials');
        if ($request->query('oauth_token') && $request->query('oauth_verifier') && $sessionTempCredentials) {
            $temporaryCredentials = unserialize($sessionTempCredentials);
            $tokenCredentials = $this->server->getTokenCredentials($temporaryCredentials, $request->query('oauth_token'), $request->query('oauth_verifier'));
            session(['token_credentials' => serialize($tokenCredentials)]);
        } else {
            throw new Exception("Need temporary oauth credentials, oauth_token and oauth_verifier to proceed.");
        }
    }

    public function recieveData()
    {
        $params = [
            "uploadStartTimeInSeconds" => (string)(time() - (24 * 60 * 60)),
            "uploadEndTimeInSeconds" => (string)time()
        ];
        if (session('token_credentials')) {
            $tokenCredentials = unserialize(session('token_credentials'));
        } else {
            return false;
        }
        $activitySummary = $this->server->getActivitySummary($tokenCredentials, $params);
        return json_decode($activitySummary);
    }

}
