<?php

namespace App\Http\Enums;

class NamesEnum
{
    const PROJECTNAME = 'CZSK Fysieke Monitor';
    const PROJECTNAMESHORT = 'CZSK';
}