<?php

namespace App\Http\Enums;

class RolesEnum
{
    const ADMIN = 1;
    const COMMANDANT = 2;
    const HOOFDOPLEIDING = 3;
}