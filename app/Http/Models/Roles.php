<?php

namespace App\Http\Models;

use Illuminate\Support\Facades\DB;

class Roles
{
    const TABLENAME = 'roles';

    public static function getRole($roleId)
    {
        return DB::table(self::TABLENAME)->where('id', $roleId)->first();
    }

    public static function getAllLeft($roleId)
    {
        return DB::table(self::TABLENAME)->where('id', '!=', $roleId)->get();
    }

}