<?php

namespace App\Http\Models;


use Illuminate\Support\Facades\DB;

class Database
{

    public static function getAll($table)
    {
        return DB::table($table)->get();
    }

    public static function getOneById($table, $id)
    {
        return DB::table($table)->where('id', $id)->first();
    }

    public static function getSpecificColumn($table, $column)
    {
        return DB::table($table)->select('id', $column)->get();
    }
}