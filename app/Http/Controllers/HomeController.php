<?php

namespace App\Http\Controllers;

use App\GarminModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{

    private $garminModel;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->garminModel = new GarminModel();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function getPersons()
    {
        return view('overviews/singleview');
    }

    public function garminIndex()
    {
        return view('garmin.garmin');
    }

    public function garminGetData()
    {
        $url = $this->garminModel->getData();
        return redirect($url);
    }

    public function garminCallback(Request $request)
    {
        $this->garminModel->callback($request);
        return redirect('garmin')->with('successMessage', 'UserAccessToken is toegevoegd!');
    }

    public function garminApiTest()
    {
        $resultJSON = $this->garminModel->recieveData();
        if ($resultJSON === false) {
            return redirect('garmin')->with('errorMessage', 'Geen gebruiker gekoppeld aan de garmin API!');
        } else {
            return redirect('garmin')
                ->with('successMessage', 'Data is succesvol opgehaald!')
                ->with('JsonResponse', $resultJSON);
        }
    }
}
