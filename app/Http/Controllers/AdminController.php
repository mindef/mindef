<?php

namespace App\Http\Controllers;


use App\Http\Enums\RolesEnum;
use App\Http\Models\Database;
use App\Http\Models\Roles;
use App\Http\Models\Users;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indexAccounts()
    {
        $accountsInfo = [];
        $accounts = Database::getAll(Users::TABLENAME);
        $roles = Database::getAll(Roles::TABLENAME);
        foreach ($accounts as $account) {
            $rolesLeft = Roles::getAllLeft($account->role_id);
            $accountsInfo[$account->id]['info'] = [
                'id' => $account->id,
                'name' => $account->name,
                'username' => $account->username,
                'current_role_name' => Roles::getRole($account->role_id)->role_name
            ];
            foreach ($rolesLeft as $roleLeft) {
                $accountsInfo[$account->id]['rolesNotAssigned'][] = [
                    'account_id' => $account->id,
                    'role_id' => $roleLeft->id,
                    'role_name' => $roleLeft->role_name
                ];
            }
        }
        View::share('accountsInfo', $accountsInfo);
        View::share('roles', $roles);
        return view('admin.accounts');
    }

    public function addAccount(Request $request)
    {
        if (!empty($request->username) && !empty($request->password) && !empty($request->passwordCheck) && !empty($request->email)) {
            if ($request->password === $request->passwordCheck) {
                Users::insert($request);
                return new JsonResponse(true);
            } else {
                return new JsonResponse('password');
            }
        } else {
            return new JsonResponse(false);
        }
    }

    public function changeRole($userId, $roleId)
    {
        $user = Database::getOneById(Users::TABLENAME, $userId);
        $role = Database::getOneById(Roles::TABLENAME, $roleId);
        $adminAmount = Users::checkAdminAmount();
        if ((int)$userId === Auth::user()->id) {
            return back()->with('errorMessage', 'U kunt uw eigen rol niet aanpassen');
        }
        if (count($adminAmount) === 1 && (int)$roleId !== RolesEnum::ADMIN) {
            return back()->with('errorMessage', 'Er moet minimaal één administrator zijn in het systeem');
        } else {
            Users::changeRole($userId, $roleId);
            return back()->with('successMessage', 'Rol van ' . $user->name . ' is succesvol aangepast naar ' . $role->role_name);
        }
    }

}
