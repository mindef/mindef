<?php

use Illuminate\Support\Facades\Auth;

Auth::routes();

$this->get('/', 'HomeController@index');
$this->get('/home', 'HomeController@index')->name('homeRoute');
$this->get('/logout', 'Auth\LoginController@logout');

$this->group(['prefix' => 'garmin'], function () {
    $this->get('', 'HomeController@garminIndex')->name('garminIndex');
    $this->get('getData', 'HomeController@garminGetData')->name('getGarminData');
    $this->get('garminCallback', 'HomeController@garminCallback');
    $this->get('garminApiTest', 'HomeController@garminApiTest')->name('getTestData');
});

$this->group(['prefix' => 'overviews'], function () {
    $this->get('singleview', 'HomeController@getPersons')->name('singleview');
});

$this->group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
    $this->get('accounts', 'AdminController@indexAccounts')->name('adminAccounts');
    $this->post('addClass', 'AdminController@addClass');
    $this->post('addAccount', 'AdminController@addAccount')->name('adminAddAccount');
    $this->get('changeRole/{accountId}/{roleId}', 'AdminController@changeRole')->name('changeRole');
});
