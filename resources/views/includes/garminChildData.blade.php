<div class="row">
    <div class="col-md-12 col-md-offset-0">
        <div class="panel panel-default">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Child</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    {{ var_dump($result) }}
                </div>
            </div>
        </div>
    </div>
</div>