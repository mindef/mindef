<div class="alert alert-success alert-dismissible accountSuccessAlert alertHide">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Success!</h4>
    Account is successvol aangemaakt!
</div>

<div class="alert alert-danger alert-dismissible accountErrorAlert alertHide">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-ban"></i> Let op!</h4>
    Er is een onverwachte fout opgetreden!
</div>

<div class="alert alert-danger alert-dismissible accountPasswordErrorAlert alertHide">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-ban"></i> Let op!</h4>
    Wachtwoorden komen niet overeen.
</div>

@if(session()->has('successMessage'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> Success!</h4>
        {{ session()->get('successMessage') }}
    </div>
@elseif(session()->has('errorMessage'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Let op!</h4>
        {{ session()->get('errorMessage') }}
    </div>
@endif