<!DOCTYPE html>
<html>
@include('layouts.head')
<body class="skin-blue">
<div class="wrapper">
    @include ('layouts.header')
    @include ('layouts.nav')
    <div class="content-wrapper">
        <section class="content-header">
        </section>
        <section class="content">
            @include('layouts.alerts')
            @yield ('content')
        </section>
    </div>
    @include ('layouts.footer')

</div>
</body>
</html>