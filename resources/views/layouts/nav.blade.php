<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ \App\Http\Enums\PathsEnum::AVATAR }}" class="img-circle"
                     alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>{{ \Illuminate\Support\Facades\Auth::user()->name }}</p>
                <a href="#">
                    <i class="fa fa-circle text-success"></i>
                    Online
                </a>
            </div>
        </div>
        <form class="sidebar-form">
            <div class="input-group">
                <input autocomplete="off" id="searchBar" type="text" class="form-control" placeholder="Zoeken"/>
                <span class="input-group-btn">
                    <a id='search-btn' class="btn btn-flat">
                        <i class="fa fa-search"></i>
                    </a>
                </span>
            </div>
            <div id="dropdown-content">

            </div>
        </form>
        <ul class="sidebar-menu">
            <li class="header">Menu</li>
            <li>
                <a href="{{ route('homeRoute') }}" class="startLoadingButton"><i class="fa fa-home"></i>Dashboard</a>
            </li>
            <li>
                <a href="{{ route('singleview') }}" class="startLoadingButton"><i class="fa fa-user"></i>
                    Personen
                </a>
            </li>
            <li>
                <a href="{{ route('singleview') }}" class="startLoadingButton"><i class="fa fa-group"></i>
                    Klassen
                </a>
            </li>
            <li>
                <a href="{{ route('singleview') }}" class="startLoadingButton"><i class="fa fa-sitemap"></i>
                    Totaal
                </a>
            </li>
            <li>
                <a href="{{ route('garminIndex') }}" class="startLoadingButton"><i class="fa fa-bar-chart"></i>
                    Garmin
                </a>
            </li>

            @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Enums\RolesEnum::ADMIN)
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-lock"></i>
                        Admin Paneel
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ route('adminAccounts') }}" class="startLoadingButton"><i class="fa fa-user"></i>
                                Accounts beheren
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
        </ul>
    </section>
</aside>