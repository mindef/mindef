<head>
    <meta charset="UTF-8">
    <title>{{ \App\Http\Enums\NamesEnum::PROJECTNAME }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link href="{{ asset ("/dist/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet"
          type="text/css"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link href="https://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ asset ("/dist/assets/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset ("/dist/assets/css/skins/skin-blue.min.css")}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset ("/dist/plugins/iCheck/all.css")}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset ("/dist/plugins/datatables/dataTables.bootstrap.css") }}"
          rel="stylesheet"/>
    <link href="{{ asset ("/css/main.css") }}" rel="stylesheet" type="text/css">
    <script src="{{ asset ("/dist/plugins/jQuery/jquery-2.2.3.min.js") }}"></script>
    <script src="{{ asset ("/dist/plugins/iCheck/icheck.min.js") }}"></script>
    <script src="{{ asset ("/dist/bootstrap/js/bootstrap.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset ("/dist/assets/js/app.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset ("/dist/plugins/datatables/jquery.dataTables.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset ("/dist/plugins/datatables/dataTables.bootstrap.min.js") }}"
            type="text/javascript"></script>
    <script src="{{ asset ("/dist/plugins/chartjs/Chart.min.js") }}"></script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.16/sorting/date-eu.js"></script>
    <script src="{{ asset ("/js/app.js") }}"></script>
</head>