@extends('layouts.master')

@section('content')
    <section class="content contentBackground">
        <div class="row">
            <div>
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Klassen overzicht</h3>
                    </div>
                    <div class="box-body">
                        <br>
                        <table id="personsTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Naam</th>
                                <th>Gewicht(kg)</th>
                                <th>Lengte(cm)</th>
                                <th>Hartslag</th>
                                <th>Km</th>
                                <th>Tijd</th>
                                <th>Slaap(licht) (Uur:Min:Sec)</th>
                                <th>Slaap(Diep) (Uur:Min:Sec)</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Ron de Bakker</td>
                                    <td>103</td>
                                    <td>183</td>
                                    <td>125</td>
                                    <td>135</td>
                                    <td>123</td>
                                    <td>01:04:52</td>
                                    <td>05:32:43</td>
                                </tr>
                                <tr>
                                    <td>Anne Schipper</td>
                                    <td>160</td>
                                    <td>85</td>
                                    <td>125</td>
                                    <td>125</td>
                                    <td>125</td>
                                    <td>02:35:41</td>
                                    <td>03:05:21</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection