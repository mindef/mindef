<html>
<head>
    @include('layouts.head')
</head>
<body class="hold-transition register-page">
<div class="register-page">
    <div class="register-logo">
        <a href="{{ route('login')}} ">{{ \App\Http\Enums\NamesEnum::PROJECTNAME }}</a>
    </div>
    <div class="register-box-body">
        <p class="login-box-msg">Een nieuw account registeren</p>
        <p class="text-left" style="font-size: small">* betekend dat het een verplicht veld is</p>
        <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('firstName') ? ' has-error' : '' }}">
                <label for="firstName" class="col-md-4 control-label">Voornaam *</label>

                <div class="col-md-6">
                    <input id="firstName" type="text" class="form-control" name="firstName"
                           value="{{ old('firstName') }}" placeholder="Voornaam" required autofocus>

                    @if ($errors->has('firstName'))
                        <span class="help-block">
                            <strong>{{ $errors->first('firstName') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('surnamePrefix') ? ' has-error' : '' }}">
                <label for="surnamePrefix" class="col-md-4 control-label">Tussenvoegsel</label>

                <div class="col-md-6">
                    <input id="surnamePrefix" type="text" class="form-control" name="surnamePrefix"
                           value="{{ old('surnamePrefix') }}" placeholder="Tussenvoegsel">

                    @if ($errors->has('surnamePrefix'))
                        <span class="help-block">
                            <strong>{{ $errors->first('surnamePrefix') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                <label for="surname" class="col-md-4 control-label">Achternaam *</label>

                <div class="col-md-6">
                    <input id="surname" type="text" class="form-control" name="surname" value="{{ old('surname') }}"
                           placeholder="Achternaam" required autofocus>

                    @if ($errors->has('surname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('surname') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                <label for="gender" class="col-md-4 control-label">Geslacht *</label>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="genderLabel">
                            <input id="gender" type="radio" name="gender" class="minimal" value="male"> Man<br>
                            <input id="gender" type="radio" name="gender" class="minimal" value="female"> Vrouw
                        </label>
                    </div>

                    @if ($errors->has('gender'))
                        <span class="help-block">
                            <strong>{{ $errors->first('gender') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('phoneNumber') ? ' has-error' : '' }}">
                <label for="phoneNumber" class="col-md-4 control-label">Telefoonnummer *</label>

                <div class="col-md-6">
                    <input id="phoneNumber" type="text" class="form-control" name="phoneNumber"
                           value="{{ old('phoneNumber') }}" placeholder="Telefoonnummer" required autofocus>

                    @if ($errors->has('phoneNumber'))
                        <span class="help-block">
                            <strong>{{ $errors->first('phoneNumber') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 control-label">E-Mailadres *</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email"
                           value="{{ old('email') }}" placeholder="E-Mailadres" required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                <label for="username" class="col-md-4 control-label">Gebruikersnaam *</label>

                <div class="col-md-6">
                    <input id="username" type="text" class="form-control" name="username"
                           value="{{ old('username') }}" placeholder="Gebruikersnaam" required>

                    @if ($errors->has('username'))
                        <span class="help-block">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-4 control-label">Wachtwoord *</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control" name="password"
                           placeholder="Wachtwoord" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="password-confirm" class="col-md-4 control-label">Wachtwoord Bevestigen *</label>

                <div class="col-md-6">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                           placeholder="Wachtwoord Bevestigen" required>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">
                        Registreren
                    </button>
                    <a href="{{ route('login') }}" type="button" class="btn btn-primary btn-block btn-flat">
                        Annuleren
                    </a>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>