@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            {{ var_dump(session('token_credentials')) }}
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Garmin</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">
                            <a href="{{ route('getGarminData') }}" class="btn btn-primary btn-flat">
                                UserAccessToken toevoegen
                            </a>
                            <a href="{{ route('getTestData') }}" class="btn btn-danger btn-flat">
                                Data ophalen
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(session()->has('JsonResponse'))
            @include('includes.garminHeadData')
        @endif
    </div>
@endsection
