$(window).load(function () {
    checkAlerts();
    $('#logoutUser').on('click', function () {
        document.location = '/logout';
    });

    $('#mindefBold').on('click', function () {
        document.location = '/login';
    });

    $('#addAccountForm').on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        var empty = form.find(".required").filter(function () {
            return this.value === "";
        });
        if (!empty.length) {
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize()
            }).done(function (result) {
                if (result !== false) {
                    if (toString(result) === 'password') {
                        localStorage.setItem('addAccountPassword', 'false');
                        location.reload();
                    } else {
                        localStorage.setItem('addAccount', 'true');
                        location.reload();
                    }
                } else {
                    localStorage.setItem('addAccount', 'false');
                    location.reload();
                }
            });
        } else {
            form.find('.required').each(function () {
                if (!this.value) {
                    $(this).css('border', '1px solid red');
                    $(this).parent().find('.alertHide').show();
                } else if (this.value) {
                    $(this).css('border', '1px solid grey');
                    $(this).parent().find('.alertHide').hide();
                }
            });
        }
    });

});

function checkAlerts() {
    var addAccount = localStorage.getItem('addAccount');
    var addAccountPassword = localStorage.getItem('addAccountPassword');
    var alertSuccess = $('.alert-success');
    var alertError = $('.alert-danger');

    if (addAccount == 'true') {
        $('.accountSuccessAlert').fadeIn().delay(2000).fadeOut('slow');
        localStorage.setItem('addAccount', null);
    } else if (addAccount == 'false') {
        $('.accountErrorAlert').fadeIn().delay(2000).fadeOut('slow');
        localStorage.setItem('addAccount', null);
    }

    if (addAccountPassword == 'false') {
        $('.accountPasswordErrorAlert').fadeIn().delay(2000).fadeOut('slow');
        localStorage.setItem('addAccountPassword', null);
    }

    if (alertSuccess.length) {
        alertSuccess.delay(3000).fadeOut('slow');
    }

    if (alertError.length) {
        alertError.delay(3000).fadeOut('slow');
    }
}

$(function () {
    $('input.rememberMeBox').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });

    $('#table').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "columnDefs": [
            {orderable: false, targets: 'noSort'},
            {type: 'date-eu', targets: 'dateSort'}
        ],
        "aaSorting": [],
        "language": {
            "sProcessing": "Bezig...",
            "sLengthMenu": "_MENU_ resultaten weergeven",
            "sZeroRecords": "Geen resultaten gevonden",
            "sInfo": "_START_ tot _END_ van _TOTAL_ resultaten",
            "sInfoEmpty": "Geen resultaten om weer te geven",
            "sInfoFiltered": " (gefilterd uit _MAX_ resultaten)",
            "sInfoPostFix": "",
            "sSearch": "Zoeken:",
            "sEmptyTable": "Geen resultaten aanwezig in de tabel",
            "sInfoThousands": ".",
            "sLoadingRecords": "Een moment geduld aub - bezig met laden...",
            "oPaginate": {
                "sFirst": "Eerste",
                "sLast": "Laatste",
                "sNext": "Volgende",
                "sPrevious": "Vorige"
            },
            "oAria": {
                "sSortAscending": ": activeer om kolom oplopend te sorteren",
                "sSortDescending": ": activeer om kolom aflopend te sorteren"
            }
        }
    });
});