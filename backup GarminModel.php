<?php

namespace App;

use http\Exception;
use Illuminate\Database\Eloquent\Model;

class GarminModelBackup extends Model
{

    private $key = "94be6ac0-55eb-4812-b433-6ada383e95eb";
    private $token = "JmcMgNzLUkmFV0CS4n0KYWeVlM5ETtcWRE9";
    private $base = 'http://connectapi.garmin.com/oauth-service-1.0/oauth';

    public function getData()
    {
        $result = $this->makeRequest("/request_token", '', 'GET');
        return $result;
    }

    private function makeRequest($input, $data = array(), $method = "")
    {
        function nonce($length = 10)
        {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        function signature()
        {
            $base_string = 'http://connectapi.garmin.com/oauth-service-1.0/oauth/request_token?oauth_consumer_key=94be6ac0-55eb-4812-b433-6ada383e95eb&oauth_signature_method=HMAC-SHA1&oauth_timestamp=' . (string)time() . '&oauth_nonce=' . nonce() . '&oauth_version=1.0';
            $temp_string = 'POST&' . urlencode($base_string);
            //$base_string = 'POST&https%3A%2F%2Fconnectapi.garmin.com%2Foauth-service-1.0%2Foauth%2Frequest_token&oauth_consumer_key%3D94be6ac0-55eb-4812-b433-6ada383e95eb%26oauth_nonce%3D'. nonce() .'%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D'. (string)time() .'%26oauth_version%3D1.0';
            $key = 'JmcMgNzLUkmFV0CS4n0KYWeVlM5ETtcWRE9&';
//            $signature = hash_hmac('sha1', $base_string, $key);
            $signature = base64_encode(hash_hmac('sha1', $temp_string, $key, true));
            $signature = urlencode($signature);
//            dd($base_string . '&oauth_signature=' . $signature);
            return $base_string . '&oauth_signature=' . $signature;
        }

//        dd(signature());
        $url = $this->base . $input;
        $sendData = "";
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $sendData .= "&" . $key . "=" . $value;
            }
        }
        if (strpos($input, "?") > 0) {
            $suffix = '&key=' . $this->key . '&token=' . $this->token;
        } else {
            $suffix = '?key=' . $this->key . '&token=' . $this->token;
        }
        $url = $url . $suffix;
//        dd(signature());
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, signature());
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
//        curl_setopt($ch, CURLOPT_HEADER, array(
//            'Authorization: OAuth oauth_version="1.0", oauth_consumer_key="94be6ac0-55eb-4812-b433-6ada383e95eb", oauth_timestamp="' . (string)time() . '", oauth_nonce="' . nonce() . '", oauth_signature_method="HMAC-SHA1", oauth_signature="' . signature() . '"'
//        ));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: OAuth oauth_version="1.0", oauth_consumer_key="94be6ac0-55eb-4812-b433-6ada383e95eb", oauth_timestamp="' . (string)time() . '", oauth_nonce="' . nonce() . '", oauth_signature_method="HMAC-SHA1", oauth_signature="' . signature() . '"'
        ));
        if (strlen($sendData) > 1) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $sendData);
        }
        if ($method == "PUT" || $method == "put") {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        }
        if ($method == "DELETE" || $method == "delete") {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        }
        curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $output = curl_exec($ch);
        $request = curl_getinfo($ch, CURLINFO_HEADER_OUT);
        dd($request);
        $error = curl_error($ch);
        //echo $error;
        curl_close($ch);
        //echo $output;
        if (strlen($error) > 0) {
            echo "De volgende fout heeft zich voorgedaan: " . $error;
            die();
        }
        try {
            return json_decode($output);
        } catch (Exception $e) {
            return $output;
        }

    }
}
